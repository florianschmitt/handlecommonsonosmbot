#!/usr/bin/env python3

from __future__ import annotations

from concurrent.futures import ThreadPoolExecutor
from contextlib import nullcontext, suppress
from enum import Enum
import inspect

import pywikibot
from pywikibot import pagegenerators
import mwparserfromhell
from pywikibot.pagegenerators import PagePilePageGenerator, SubpageFilterGenerator, PreloadingGenerator

import overpy
import re
import time
from operator import contains
from overpy.exception import OverpassGatewayTimeout
from urllib.error import URLError

from pywikibot.bot import (
    ExistingPageBot,
    SingleSiteBot,
)

docuReplacements = {'&params;': pagegenerators.parameterHelp}  # noqa: N816

class WdProperties(Enum):
    OBJECTLOCATION  = 'P9149'
    LOCATION        = 'P1259'
    STATEDIN        = 'P248'
    OPENSTREETMAP   = 'Q936'
    OSMWAYID        = 'P10689'
    OSMNODEID       = 'P11693'
    OSMRELID        = 'P402'

class OsmImg:
    """
    Helper Class to encapsulate Image-related information: Commons file Name and Overpass Turbo Query string
    """
    def __init__(self, file: str, attribute: str):
        """
        Default constructor.

        Parameters:
            file (str):       The Commons File page name (without Namespace).
            attribute (str):  The OSM attribute referencing this file. E.g. wikiimedia_commons=File:abj.jpg.
        """
        self.file: str = 'File:' + file
        self.attribute: str = attribute.split('=', 1)[0]
        self.value:str = attribute.split('=', 1)[1]
        # self.myval = self.value.replace('"','\\"').replace("'","\\'").replace(' ','_')
        self.myval:str = self.value.replace('"','\\"').replace("'","\\'")
    def __repr__(self):
        return f'nwr[\"{self.attribute}\"=\'{self.myval}\'];out center;'

class HandleCommonsOnOSMBot(ExistingPageBot, SingleSiteBot):
    """
    Custom Pywikibot Class to enrich Commons File pages with {{Object location}} from Openstreetmap
    and with an {{OSMLink}}.

    Class Variables:
        infoTemplates: List of Information templates or derived templates.
        locationTemplates: List of location-related templates. If present, Bot won't add a location
        osmLinkTemplates: List of templates linking to Openstreetmap. If present, Bot won't add {{OSMLink}}.
        api: Overpass API instance from Overpy.
    """

    infoTemplates: list[str]        = ['information', 'photograph', 'artwork', 'art photo']
    locationTemplates: list[str]    = ['object location', 'object location dec']
    osmLinkTemplates: list[str]     = ['osmlink', 'on osm']
    api = overpy.Overpass()

    @staticmethod
    def hasNWR(result: overpy.Result) -> bool:
        """ 
        Static method to check if a Overpass API result contains Nodes, Ways or relations.

            Parameters:
                result: Overpass API Result object
        """
        return bool(result.nodes) or bool(result.ways) or bool(result.relations)

    @staticmethod
    def create_loc_template(result: overpy.Result) -> str:
        """ Static Method to build a {{Object location}} template for a given Overpass API result."""
        """
        if len(result.nodes) > 0:
            lat = result.nodes[0].lat
            lon = result.nodes[0].lon
        elif len(result.ways) > 0:
            lat = result.ways[0].center_lat
            lon = result.ways[0].center_lon
        elif len(result.relations) > 0:
            lat = result.relations[0].center_lat
            lon = result.relations[0].center_lon
        tmplstr: str = '{{Object location|' + str(lat) + '|' + str(lon) + '|source:OSM}}'
        """
        tmplstr: str = '{{Object location}}'
        return tmplstr

    @staticmethod
    def create_coords_claim(result: overpy.Result, site: pywikibot.Site, prop: str = 'P9149') -> pywikibot.Claim:
        if len(result.nodes) > 0:
            lat = result.nodes[0].lat
            lon = result.nodes[0].lon
        elif len(result.ways) > 0:
            lat = result.ways[0].center_lat
            lon = result.ways[0].center_lon
        elif len(result.relations) > 0:
            lat = result.relations[0].center_lat
            lon = result.relations[0].center_lon

        if lat == None or lon == None:
            return None

        # Create "coordinates of depicted place" claim (P9149) which correspondents to Object location:
        coordinateclaim  = pywikibot.Claim(site, prop)        
        coordinate = pywikibot.Coordinate(lat=float(lat), lon=float(lon), precision=0.00001, site=site)
        coordinateclaim.setTarget(coordinate)

        # add "stated in" Property (P248) with Value "OpenStreetMap" (Q936) as source claim
        sourceclaim = pywikibot.Claim(site, 'P248')
        sourceclaim.setTarget(pywikibot.ItemPage(pywikibot.Site('wikidata'), "Q936"))
        coordinateclaim.addSource(sourceclaim)

        return coordinateclaim

    @staticmethod
    def create_osm_claim(result: overpy.Result, site: pywikibot.Site) -> pywikibot.Claim:
        """ Static Method to build a SDC OSM ID claim for a given Overpass API result."""
        if len(result.nodes) > 0:
            id = result.nodes[0].id
            type = "node"
            prop = WdProperties.OSMNODEID.value
        elif len(result.ways) > 0:
            id = result.ways[0].id
            type = "way"
            prop = WdProperties.OSMWAYID.value
        elif len(result.relations) > 0:
            id = result.relations[0].id
            type = "relation"
            prop = WdProperties.OSMRELID.value

         # Create "coordinates of depicted place" claim (P9149) which correspondents to Object location:
        osmclaim  = pywikibot.Claim(site, prop)        
        osmclaim.setTarget(str(id))
        return osmclaim

    @staticmethod
    def create_osmlink_template(result: overpy.Result) -> str:
        """ Static Method to build a {{OSMLink}} template for a given Overpass API result."""
        if len(result.nodes) > 0:
            id = result.nodes[0].id
            type = "node"
        elif len(result.ways) > 0:
            id = result.ways[0].id
            type = "way"
        elif len(result.relations) > 0:
            id = result.relations[0].id
            type = "relation"
        tmplstr: str = "{{OSMLink|type=" + type + "|OSM_ID=" + str(id) + "}}"
        return tmplstr
    
    @staticmethod
    def hasListedTemplate(cfcode: Wikicode, templatelist: list[str]) -> bool:
        """
        Generic static method to check wikicode for templates (case-insensitive).

            Parameters:
                cfcode:         Wikicode content of current filepage;
                templatelist:   List of lower-case template names to check for
            
            Returns:
                templateFound:  boolean true if one of the template names was 
                                found somewhere in the Wikicode.
        """
        templateFound: bool = False
        for template in cfcode.filter_templates():
            # if (template.name.matches("Object location") or template.name.matches("Location")):
            if contains(templatelist, template.name.strip().lower()):
                templateFound = True
        return templateFound

    @staticmethod
    def avoid_manual_reverts(filepage: pywikibot.page.FilePage):
        """
            Helper function to be called before any potential file page edit (after checking
            the presence of the Location / OSMLink templates).
            
            Checks the revision history for previous bot edits. If edits with the bot's
            default edit comment are found, we assume that those edit has been reverted,
            since otherwise, the templates should be presend. If a revert of the bot's
            work was detected this way, we won't touch the file page's text, but we may
            continue with SDC edits.

            Parameters:
                filepage:               Commons filepage to check for bot's edits.
            
            Returns:
                avoid_manual_reverts:   boolean true if reverted edit was found; false
                                        if bot can continue its work.
        """        
        # Have we seen this page before? If not, continue working on it...
        contributors = filepage.contributors()
        if 'HandleCommonsOnOSMBot' not in list(contributors.elements()):
            pywikibot.logging.info("Check manual reverts: haven't seen page {0} yet! Goin' on...".format(filepage.title()))
            return False

        # We have worked on this page previously: get revision history for our edits
        revisions = filepage.revisions()
        myedits = [x for x in list(revisions) if x['user'] == 'HandleCommonsOnOSMBot']

        # Check previous edit's comment
        hit: re.Match = None
        for m in myedits:
            hit = re.search(r"Added \[\[Template:Object location", m['comment'])
            if hit != None:
                pywikibot.logging.info(
                    "Previous template edit on {0} from {1} found!".format(
                            filepage.title(),
                            m['timestamp']
                ))
                break

        return hit != None

    def treat_page(self) -> None:
        """
            treat_page() - custom method to cverride pywikibots default treat_page().

            Called for every page of the input pagepile, thus, for every gallery subpage
            of Commons:Files used on OpenStreetMap.

            For every gallery page, we create a dictionary of OsmImg objects as values
            and File page titels as keys. This "preprocessing" is required since the
            gallery entries may contain outdated file names, URLs in different variants
            and so on.

            After "normalizing" the gallery content, we iterate over each entry to check
            if there's something to do for the bot (add templates and / or SDC).
        """

        i = inspect.getgeneratorlocals(self.generator)

        # Images dictionary: lists all images (Instances of OsmImg class)
        images: dict[OsmImg] = {}

        # basepage = pywikibot.Page(pywikibot.Site(), 'Commons:Files used on OpenStreetMap/11')
        basepage: pywikibot.Page = self.current_page

        # for page in PagePilePageGenerator(58502):
        
        pywikibot.logging.info('===============================================')
        pywikibot.logging.info(f'Analyzing {basepage.title()}...')
        pywikibot.logging.info('===============================================')
        tree: Wikicode = mwparserfromhell.parse(basepage.text)

        # Parse the gallery of our basepage and fill the images dictionary.
        # Structure of gallery:
        # FilePageName|[taginfo1url taginfo1text](<br/>[taginfo2url taginfo2text])?
        # - FilePageName> may consist of multiple files, separated by ";";
        # - taginfo link may appear multiple times (separated by <br/>), if the same
        #   file is references in different attributes.
        # We'll work only on the first file / OSM attribute.
        for gall in [tag for tag in tree.filter_tags()
                        if tag.tag == "gallery"]:
            gen = (x for x in gall.contents.splitlines() if len(x) > 3)
            for line in gen:
                galleryimgname = line.split('|')[0]
                imgnames = re.split(';File:', galleryimgname)
                if len(imgnames) > 1:
                    time.sleep(1)
                    pywikibot.logging.info(f'Multiple file references found in line {line}! Working only on {imgnames[0]}...')
                osmEntries = line.split('|')[1].split("<br/>")
                if len(osmEntries) > 1:
                    pywikibot.logging.info(f'Multiple OSM attribute entries found in line {line}! Working only on first entry...')
                i = OsmImg(
                    imgnames[0], 
                    osmEntries[0].split(' ', 1)[1].replace("]", "")
                )
                images[i.file] = i

        # Iterate over gallery images (File: pages) using the imagelinks
        links = [page for page in basepage.imagelinks()
                 if page.exists()]
        for filepage in PreloadingGenerator(links, groupsize=50):
            # Try to get valid file page name in case of broken links
            if re.search(';File:', filepage.title()):
                oldtitle = filepage.title()
                newtitle = filepage.title().split(';File:')[0]
                filepage = pywikibot.Page(pywikibot.Site(), newtitle)
                pywikibot.logging.warning(f'Filepage changed from: {oldtitle} to {newtitle}...')

            if not filepage.is_filepage():
                pywikibot.logging.warning(f'{filepage.title()} is not a valid filepage, skipping...')
                continue
                       
            # Initialize variables for current File: page
            orig_name: str          = ''
            addLocation: bool       = False
            addOsmLink: bool        = False
            hasInfoTempl: bool      = False
            result: overpy.Result   = ''
            
            
            # Handle redirects. From the gallery, we sometimes get the old filename
            # which we still need to access our images dictionary. If the gallery already
            # uses the redirect target, we can ignore the redirect
            for bl in filepage.backlinks(filter_redirects=True):
                pywikibot.logging.info(f'Redirect from {bl.title()} to {filepage.title()}!')
                # check if the filepage title is in our images collection. If not, we need to
                # use the old name as key.
                try:
                    if not images[filepage.title()]:
                        orig_name = bl.title()
                except KeyError:
                    orig_name = bl.title()
                break

            # Make absoultely sure we don't work on redirect pages!
            if filepage.isRedirectPage():
                orig_name = filepage.title()
                filepage = filepage.getRedirectTarget()

            # Parse page content.
            cfcode: Wikicode = mwparserfromhell.parse(filepage.text)

            # Check for Information template.
            hasInfoTempl = self.hasListedTemplate(cfcode, self.infoTemplates)

            # Check what to do
            addOsmLink = not self.hasListedTemplate(cfcode, self.osmLinkTemplates)
            addLocation = not self.hasListedTemplate(cfcode, self.locationTemplates)

            # If there's something to do:
            if (addLocation or addOsmLink):

                # Check and respect blacklisting
                blacklisted: bool = not filepage.botMayEdit()
                if blacklisted:
                    pywikibot.logging.info(
                        "Skipping {0} (blacklist: {1})".format(
                                filepage.title(), blacklisted
                        ))
                    continue

                # Check if we should refrain from adding temples to avoid manual reverts
                avoidreverts: bool = False
                if hasInfoTempl:
                    avoidreverts = self.avoid_manual_reverts(filepage)

                pywikibot.logging.info('------------------------------------------------------------------------------')
                pywikibot.logging.info(f"""Working on {filepage.title()}:
                                Full URL: {filepage.full_url()}
                                addLocation: {addLocation}
                                addOsmLink: {addOsmLink}
                                avoidreverts: {avoidreverts}""")

                try:
                    if orig_name == '':
                        Img: OsmImg = images[filepage.title()]
                    else:
                        Img: OsmImg = images[orig_name]
                except KeyError:
                    pywikibot.logging.warning(f'KeyError catched, leaving {filepage.title()} untouched!')
                    continue

                # Query Overpass API for our current img, build templates from result. 
                overpassloops = 0
                while result == '':
                    try:
                        overpassloops += 1
                        result = self.api.query(repr(Img))
                    except OverpassGatewayTimeout:
                        pywikibot.logging.warning(f'OverpassGatewayTimeout catched, sleeping for {overpassloops * 15} secs...')
                        time.sleep(overpassloops * 15)
                    except URLError:
                        pywikibot.logging.warning(f'URLError catched, sleeping for {overpassloops * 15} secs...')
                        time.sleep(overpassloops * 15)

                if self.hasNWR(result):
                    pywikibot.logging.info(f'Valid result from Overpass API: {result!s}...')
                    loc_template: str       = self.create_loc_template(result)
                    coords_claim: pywikibot.Claim = self.create_coords_claim(result, pywikibot.Site(), 'P9149')
                    osm_claim: pywikibot.Claim = self.create_osm_claim(result, pywikibot.Site())
                    osmlink_template: str   = self.create_osmlink_template(result)
                else:
                    pywikibot.logging.warning(f'No valid result from Overpass API: {result!s}, skipping {filepage.title()}')
                    continue
                
                # Prepare SDC edit: get MediaInfo for FilePage
                mediaInfo: pywikibot.MediaInfo = filepage.data_item()
                item_dict = mediaInfo.get()
                claimlist = []
                summary = "Adding SDC Claims: "

                # Apply the Object location template after Information template
                if addLocation:

                    # Add SDC location
                    if WdProperties.OBJECTLOCATION.value in item_dict.get('statements'):
                        pywikibot.logging.warning('SDC Object location found, leaving untouched')
                    else:
                        if coords_claim != None:
                            # mediaInfo.addClaim(coords_claim, summary="Added SDC object location.")
                            claimlist.append(coords_claim.toJSON())
                            summary += "Object Location - "
                            # pywikibot.logging.info('SDC object location successfully added')
                    
                    # If filepage has infotemplate, add Object location template
                    if hasInfoTempl:
                        if avoidreverts:
                            pywikibot.logging.info(
                                "Don't add Location template on {0} (avoid manual reverts: {1})".format(
                                filepage.title(), avoidreverts
                            ))
                        else:
                            for template in cfcode.filter_templates():
                                if contains(self.infoTemplates, template.name.strip().lower()):
                                    cfcode.insert_after(template, '\n' + loc_template)
                                    break

                # add OSMLink Template (requires InformationTemplate!)
                if addOsmLink:

                    # Add SDC OSM Link
                    if (
                        WdProperties.OSMNODEID.value in item_dict.get('statements') or
                        WdProperties.OSMRELID.value in item_dict.get('statements') or 
                        WdProperties.OSMWAYID.value in item_dict.get('statements')
                        ):
                        pywikibot.logging.warning('SDC OSM ID found, leaving untouched')
                    else:
                        # mediaInfo.addClaim(osm_claim, summary="Added SDC OSM ID.")
                        claimlist.append(osm_claim.toJSON())
                        summary += "OSM ID - "
                        # pywikibot.logging.info('SDC OSM ID successfully added')

                    # add OSMLink template:
                    if hasInfoTempl:
                        if avoidreverts:
                            pywikibot.logging.info(
                                "Don't add OSMLink template on {0} (avoid manual reverts: {1})".format(
                                filepage.title(), avoidreverts
                            ))
                        else:
                            for template in cfcode.filter_templates():
                                if contains(self.infoTemplates, template.name.strip().lower()):
                                    for param in template.params:
                                        if re.match("\\s?[O|o]ther[\\s|_]fields\\s?", str(param.name)):
                                            cfcode.insert_after(param.value, "\n"+ osmlink_template + "\n")
                                            addOsmLink = False
                                            break
                                    if addOsmLink:
                                        template.add("other fields", osmlink_template)
                                        addOsmLink = False
                                    break
                for template in cfcode.filter_templates():
                    if template.name.matches('location possible'):
                        cfcode.remove(template)
                        pywikibot.logging.info('Template {{Location possible}} found and removed...')

                # Store SDC if there's someting to set:
                if len(claimlist) > 0:
                    # datarepo = site.DataSite('commons', 'commons')
                    media_identifier = 'M{}'.format(filepage.pageid)
                    claimdict = { 'claims': claimlist}
                    site = pywikibot.Site('commons', 'commons')
                    site.editEntity({'id': media_identifier }, claimdict, summary=summary)
                    pywikibot.logging.info('SDC successfully added')

                # Store new page text in case of modifications:
                if str(cfcode) == filepage.text:
                    pywikibot.logging.info('Page text not modififed.')
                else:
                    pywikibot.logging.info('Page text modififed, storing...')
                    filepage.put(str(cfcode), 'Added [[Template:Object location|{{Object location}}]] and/or [[Template:OSMLink|{{OSMLink}}]] templates.')

            else:
                pywikibot.logging.info(f'Nothing to do for HandleCommonsOnOSMBot on {filepage.title()}...')
            
            # end of modification if clause
        # end of iteration over filepages
    # end of treat_page() method

def main(*args: str) -> None:
    """Parse command line arguments and invoke bot."""
    options = {}
    gen_factory = pagegenerators.GeneratorFactory()
    
    # Option parsing
    local_args = pywikibot.handle_args()  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options

    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value

    HandleCommonsOnOSMBot(generator=gen_factory.getCombinedGenerator(preload=True), **options).run()

if __name__ == '__main__':
    main()
