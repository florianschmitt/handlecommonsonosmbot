![GitLab language count](https://img.shields.io/gitlab/languages/count/florianschmitt%2FHandleCommonsOnOSMBot?gitlab_url=https%3A%2F%2Fgitlab.wikimedia.org)
![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/florianschmitt%2FHandleCommonsOnOSMBot?gitlab_url=https%3A%2F%2Fgitlab.wikimedia.org)
![GitLab Issues](https://img.shields.io/gitlab/issues/open/florianschmitt%2FHandleCommonsOnOSMBot?gitlab_url=https%3A%2F%2Fgitlab.wikimedia.org)![GitLab Release](https://img.shields.io/gitlab/v/release/florianschmitt%2FHandleCommonsOnOSMBot?gitlab_url=https%3A%2F%2Fgitlab.wikimedia.org)

![GitLab License](https://img.shields.io/gitlab/license/florianschmitt%2FHandleCommonsOnOSMBot?gitlab_url=https%3A%2F%2Fgitlab.wikimedia.org)

# HandleCommonsOnOSMBot

## Description

Public source for [HandleCommonsOnOSMBot](https://commons.wikimedia.org/wiki/User:HandleCommonsOnOSMBot) on Wikimedia Commons. HandleCommonsOnOSMBot adds [{{Object location}}](https://commons.wikimedia.org/wiki/Template:Object_location) and [{{OSMLink}}](https://commons.wikimedia.org/wiki/Template:OSMLink) templates for Commons Files that are used on OpenStreetMap (using attributes [`wikimedia_commons`](https://wiki.openstreetmap.org/wiki/Key:wikimedia_commons) or [`image`](https://wiki.openstreetmap.org/wiki/Key:image)). Insofar, HandleCommonsOnOSMBot relies on the work of [Usage Bot](https://commons.wikimedia.org/wiki/User:Usage_Bot) and goes through the media listes on the Files used on OpenStreetMap pages.

For open tasks/issues/bug reports, see [phabricator](https://phabricator.wikimedia.org/project/profile/7301/).

HandleCOmmonsOnOSMBot respects blacklisting using the [{{bots}} template](https://commons.wikimedia.org/wiki/Template:Bots). To prevent HandleCommonsOnOSMBot from editing a filepage, place `{{bots|deny=HandleCommonsOnOSMBot}}` anywhere on the page, or add it to an existing blacklist.

Additionally, HandleCommonsOnOSMBot tries to avoid "manual reverts". Before adding templates to the file description, it checks the page history for earlier modifications with a comment beginning like "Adding `{{Object location}}`and/or `{{OSMLink}}`...". If there are such entries, the bot will refrain from adding any templates.

## Details and Limitations

* HandleCommonsOnOSMBot **won't** add `{{Object location}}` if there's either an `{{Object location}}` or `{{Location}}` template present, regardless of its content. Currently, HandleCommonsOnOSMBot's sole task is to add missing data, not to emend existing geolocation data.
* HandleCommonsOnOSMBot **will** add `{{Object location}}` if there's one of the deprecated/experimental location templates present like `{{Object location dec}}` or similar. It won't replace those deprecated/experimental templates since there's no obviuous consensus on that point yet.
* HandleCommonsOnOSMBot **won't** add an `{{OSMLink}}` if there's a `{{On OSM}}` template present. Even if `{{OSMLink}}` seems to be the better choice for `File:` pages, there's no consensus yet to replace `{{On OSM}}` there.

### Commons-related limitations ###

HandleCommonsOnOSMBot will edit a File: page only if there's either an `{{Information}}` template or one of some other templates present. Otherwise, he doesn't know where to place his additions, thus those pages will be skipped. Besides `{{Information}}`, HandleCommonsOnOSMBot currently recognizes the following templates:

* `{{Photograph}}`
* `{{Artwork}}`
* `{{Art photo}}`

### OpenStreetMap-related limitations ###

Currently, HandleCommonsOnOSMBot isn't able to handle files that are **used multiply** on OpenStreetMap (on *different* OSM entities), for example [File:Mangerstraße 8 Potsdam.jpg](https://commons.wikimedia.org/wiki/File:Mangerstra%C3%9Fe_8_Potsdam.jpg) (used on https://www.openstreetmap.org/way/96850637 and https://www.openstreetmap.org/relation/11178004). In this case, the best solution would be to add `{{OSMLink}}`s for each OSM entity; the position may differ, but since absolute precision isn't possible, we'll simply use the coordinates of the first OSM object found.

HandleCommonsOnOSMBot is now able to handle files which are **referenced by the *same map feature*** using both `wikimedia_commons` and `image` attributes* (for example, [File:2009-05 Seddin 2.jpg](https://commons.wikimedia.org/wiki/File:2009-05_Seddin_2.jpg) referenced twice by https://www.openstreetmap.org/way/445939754). It will simply process the first OSM attribute it encounters and ignore other attributes.

Currently, HandleCommonsOnOSMBot can't handle **special OSM tagging** where the `wikimedia_commons` or `image` attributes have *prefixes* (like `artist:wikimedia_commons`, see https://taginfo.openstreetmap.org/keys/artist:wikimedia_commons) or *suffixes* (like `wikimedia_commons:4`, see https://taginfo.openstreetmap.org/keys/wikimedia_commons%253A4).

In rare cases, OSM objects refer to multiple Commons files in one single `wikimedia_commons` attribute, despite this is [discouraged in the OSM wiki](https://wiki.openstreetmap.org/wiki/Key:wikimedia_commons#Usage). In those cases, HandleCommonsOnOSMBot will work only on the first of those files. There's a [Maproulette Challenge](https://maproulette.org/browse/challenges/48753) to split the Commons file references into multiple OSM attributes. Help is highly appreciated.

## Dependencies

HandleCommonsOnOSMBot requires the following packages:

* [pywikibot](https://github.com/wikimedia/pywikibot) - [Documentation](https://doc.wikimedia.org/pywikibot/stable/);
* [mwoauth](https://pypi.org/project/mwoauth/) (optional, recommended; see [Mewdiawiki Docs](https://www.mediawiki.org/wiki/Manual:Pywikibot/OAuth));
* [overpy](https://pypi.org/project/overpy/).
